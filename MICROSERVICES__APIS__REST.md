## MICROSERVICES, APIS, REST

**Client-Server Architecture Defined**
>**Client-server architecture:** Clients are programs whose specialty is asking servers for information and (usually)
allowing the user to interact with that information, and servers are programs whose specialty
is efficiently serving large numbers of clients simultaneously.
## Special Note:
In contrast to the client software, which is typically a discrete app running on a single
device such as a PC or smartphone, the “server” is in fact typically a collection of computers running multiple different software components (which we will meet in due time) that
together comprise the functionality of the actual site.
- Distinguishing clients from servers allows each type of program to be highly specialized
to its task:
- In the case of client-server architectures, what stays the
same is the separation of concerns between the client and the server, despite changes across
implementations of clients and server.

>Summary of Service-Oriented Architecture and Microservices
• Although the term was nearly lost in a sea of confusion, Service Oriented Architecture (SOA) just means an approach to software development in which subsystems can only access each others’ data via external interfaces.
