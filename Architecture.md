### Module Overview SaaS Architecture
>Berlely University of California
Module Overview SaaS Arcitecture
Software Enginnering  

**History of Browser: _from 1973-2021_** 
| Start | End |
| ------ | ------ |
| 1973 | 1983 |
| 1984 | 2021|

>Before the web became a mass market phenomenon in the mid 1990s, it consisted mostly of static pages with text and graphics. 

### Purpose of this Module**
This module tries to illuminate the two main trajectories of the 25 year journey that led from static pages to microservices and Restful APIs or application programming interfaces. 

* The first trajectory is an intellectual. 
  
 >To oversimplify just a little, developers stopped thinking about the web as a collection of pages delivered to a universal viewer and started thinking about it as a collection of programs that could be invoked remotely to return any kind of data, not just viewable web page. 
  * Introduced the remote procedure call 
  * Transformed the web into a mesh of services

* The second trajectory has to do with standards adoption and holds some valuable lessons for technologists. 

> The original Hypertext Transfer Protocol, or HTTP, was never designed to support remote procedure call. 

* In fact, it wasn't even designed to support the concept of a web session, such as remembering whether you're logged in or not. 
 > ### HTTP was simple enough that it spread very quickly and good enough that it would be too difficult to displace. Became the standard. 

 * The winner is most likely to be a simple solution that's easy for developers to understand and work with. 
 * Works well with existing deployed software and doesn't require individual developers to make licensing arrangements to use it. 

## Goal of QA reviewing the Module
> Is to learn about:
> *  Service oriented architecture
> * Microservices
> * Resources and restful APIs 
