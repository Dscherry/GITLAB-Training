# GITLAB Training
Doc_Code
### *Documents as Code*
Author:	Eric Holscher & the Write the Docs community

>Documentation as Code (Docs as Code) refers to a philosophy that you should be writing documentation with the same tools as code

- Issue Trackers
- Version Control (Git)
- Plain Text Markup (Markdown, reStructuredText, Asciidoc)
- Code Reviews
- Automated Tests

> This means following the same workflows as development teams, and being integrated in the product team. It enables a culture where writers and developers both feel ownership of documentation, and work together to make it as good as possible.

Generally a Docs as Code approach gives you the following benefits:

- Writers integrate better with development teams
- Developers will often write a first draft of documentation
- You can block merging of new features if they don’t include documentation, which incentivizes developers to write about features while they are fresh

 ## Document Management System

By using a version control system like Git, the user has access to a perfect document management system for free. It version controls all documents, branch the documents and shows an audit trail. Those users with access areable to check who wrote which part of the documents. 

### Collaboration and Review Process

Git as a distributed version control system even enables document collaboration. Users can fork the docs and send  pull requests for the changes they made.
- By reviewing the pull request, the Team  has a perfect review process out of the box.  
- By accepting the pull request, each approved reviewer can review and accepted the changes. Most Git frontends like Bitbucket, GitLab and GitHub also allow approved reviewers to reject pull requests with comments.

### Goal of Documentation

**Documentation is the single source of truth (SSOT)**

- The GitLab documentation is the SSOT for all information related to GitLab implementation, usage, and troubleshooting. 
- It evolves continuously, in keeping with new products and features, and with improvements for clarity, accuracy, and completeness.

> This policy prevents information silos, making it easier to find information about GitLab products.

- It also informs decisions about the kinds of content we include in our documentation.

- The documentation includes all information

### Topic types

In the software industry, it is a best practice to organize documentation in different types. For example:

- Concepts
- Tasks
- Reference
- Troubleshooting

> We employ a documentation-first methodology. This method ensures the documentation remains a complete and trusted resource, and makes communicating about the use of GitLab more efficient.

### Document- First Methodology 

- If the answer to a question exists in documentation, share the link to the documentation instead of rephrasing the information.

- When you encounter new information not available in QA documentation (for example, when working on a support case or testing a feature), the first step should be to create a merge request (MR) to add this information to the documentation. 
- Each user can then share the MR to communicate this information.

