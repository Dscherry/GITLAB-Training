Hypertext Transfer Protocol
SaaS Communication Uses HTTP Routes
Software Engineering
**Digital Learning Service**

>It is an ASCII based request reply protocol, so let's unpack that ASCII base. 
- That means that when we're sending stuff over HTTP, we're sending plain old ASCII strings. 
- HTTP always begins with a client asking something of a server and always concludes with the server replying. 

> Unless of course the network is crashed. 

* Then the server might not reply at all, but the point is it is the client that is initiating stuff. 

* Even if the server tries to reply but gives an error that is still a reply, the server can reply saying sorry can't do that, but it is still a request qualifier reply and so kind of point number one about Agent.

### Uniform Resource identifier Uri
Sometimes it's called URL for uniform resource locator. URI because it's actually a little bit more general. 

**_Special Note:_** The UI by itself doesn't determine what is going to happen. It is the combination of the URI and the chosen request method. 
   
   -  The exchange between the Client and Server respond as if to say: 
  
 >" Which version of the HTTP protocol do I understand so that if the server and the client understand different versions of the protocol, hopefully they can agree on some things from the nominator. 
 - The client request will also include some headers, some extra information regarding what the what it wants to get from the server.
  
 - If all goes well, the server will eventually receive that message and come back with a response, which basically acknowledges yes. 
  > Overview of Exchange: 
  - Was there a problem because you're not allowed to get to that thing, or it wasn't found? 
  - Was there a problem because internally I pull up and I'm very confused and I can't reply at all. 

>The server will also include some response headers with some more information about skip this request succeeded. 