Service-Orientated Architecture

## Microservices 
Microservices is a word that w 5 years ago did not exist. 
>**History: Dynamic content:** generation life on the web used to be really simple because when the web first started the only thing that could be done was to name a URL as a static file on a disk somewhere linking to a document that HTML could render or read from. 
- Maybe the document had some images embedded in it, but pretty much there were files sitting on some computer and HTTP was just being used to transmit a request basically get a copy of those files. That's in fact how the web started. 
- It was originally a way for scientists to be able to easily publish papers that had graphics and things in that, and to be able to easily retrieve each others work. 

## Current State of the Web
> Introduction of Microservices: A bunch of sites organized  as independent functions or services.
> - Building a program just the same way as one can pull in  different Python libraries when writing Python code.

  - The call functions in those libraries, respond like existing black boxes. 
   - Microservices operate much like functions within codes libraries. 
   - The focus now becomes a service oriented architecture? 

>Because now each site function is not the most important thing when viewing the code itself the question is: 

**It's what does that site do and how could one combine what that site does with what other sites do.**
