### The Web's Client-Server Architecture
>Berlely University of California
Software Enginnering Master Document

Two sides of the coin:
Clients like web browsers are on one side of it, and servers that serve web content on the other side. 

**What does the Client Do?**
* The client begins every interaction.
* There's a bunch of clients, typically things like web browsers or maybe mobile apps. Those clients spend time talking to one or more server
* Clients are usually optimized to do a particular kind of thing, which is that they're requesting information from the server the server is optimized to serve. 
>Special Note: So the client and server, like I said, are each specialized for their tasks. 

>They could be comparably complex an as will see, client Server is an early example of a design pattern, which is a strategy that captures some common solution to a set of related problems. 

* This particular design pattern is what we would call an architectural one because it talks about from a very high level. 

### HTML and CSS
So conceptual foundations, HTML Hypertext markup language. It is a hierarchy of nested elements. 
1. The top level single element is the HTML document. Everything else nest within that. 
   
   **What kind of nuts and elements?**
   - Well, some of them describe text that could go on a page like tables and paragraphs.
   - They say this part of the page I'm going to put a virtual container around it and manipulate it later. 
   - Even if the virtual container does not itself cause any visual change in how the page is displayed. 

>Major Point: So the most common sort of schema here is that there is one or more stylesheets which will associate attributes, especially classes and IDs with visual elements that were tastefully chosen by people who understand graphic design. 

**Points about Bootstrap Left out**
